import sys
import os

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.properties import ObjectProperty
from kivy.uix.image import Image
from place import *
from component import *
from transition import *


class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)


class WindowWidgets(BoxLayout):
    def __init__(self, **kwargs):
        super(WindowWidgets, self).__init__(**kwargs)

    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    text_input = ObjectProperty(None)

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def load(self, path, filename):
        with open(os.path.join(path, filename[0])) as stream:
            self.text_input.text = stream.read()

        self.dismiss_popup()

    def save(self, path, filename):
        with open(os.path.join(path, filename), 'w') as stream:
            stream.write(self.text_input.text)

        self.dismiss_popup()

    def add_component(self):
        self.ids.main_workspace.add_widget(Component())

    def add_place(self):
        self.ids.main_workspace.add_widget(Place())

    def add_transition(self):
        # scatter = Scatter(do_rotation=False, size_hint=(None, None))
        # image = Image(source='./transition.png')
        # scatter.add_widget(image)
        self.ids.main_workspace.add_widget(Transition())

    def add_data_dependency(self):
        scatter = Scatter(do_rotation=False, size_hint=(None, None))
        image = Image(source='./data_dependency.png')
        scatter.add_widget(image)
        self.ids.main_workspace.add_widget(scatter)

    def add_service_dependency(self):
        scatter = Scatter(do_rotation=False, size_hint=(None, None))
        image = Image(source='./service_dependency.png')
        scatter.add_widget(image)
        self.ids.main_workspace.add_widget(scatter)

    # clears all widgets from workspace
    def clear_widgets(self):
        self.ids.main_workspace.clear_widgets()

    @staticmethod
    def exit_program():
        sys.exit()


class MABApp(App):
    def build(self):
        return WindowWidgets()


if __name__ == '__main__':
    MABApp().run()
